<?php

namespace App\Services\Category;

use LaravelEasyRepository\Service;
use App\Repositories\Category\CategoryRepository;

// class CategoryServiceImplement
class CategoryServiceImplement extends Service implements CategoryService
{

    /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
    protected $mainRepository;

    public function __construct(CategoryRepository $mainRepository)
    {
        $this->mainRepository = $mainRepository;
    }

    public function paginate()
    {
        return $this->mainRepository->paginate(10);
    }

    public function search_category_name_and_description($request)
    {
        return $this->mainRepository->search_category_name_and_description($request);
    }

}
