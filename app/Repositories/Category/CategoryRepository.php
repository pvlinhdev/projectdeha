<?php

namespace App\Repositories\Category;

use Illuminate\Http\Request;
use LaravelEasyRepository\Repository;

interface CategoryRepository extends Repository{

    public function paginate();
    public function search_category_name_and_description(Request $request);

}
