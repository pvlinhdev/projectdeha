<?php

namespace App\Repositories\Category;

use LaravelEasyRepository\Implementations\Eloquent;
use App\Models\Category;

class CategoryRepositoryImplement extends Eloquent implements CategoryRepository
{
    /**
     * Model class to be used in this repository for the common methods inside Eloquent
     * Don't remove or change $this->model variable name
     * @property Model|mixed $model;
     */

    protected $model;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    public function paginate()
    {
        $category = $this->model->paginate();
    }

    // search name and description
    public function search_category_name_and_description($request){

        $search = $request->input('s');

        $search = str_replace('%', '\%', $search);
        $categories = Category::where('name', 'LIKE', '%' . $search . '%')
            ->orwhere('description',  'LIKE', '%' . $search . '%')
            ->orderBy('id', 'DESC')
            ->paginate(5);

        return $categories;
    }

}
