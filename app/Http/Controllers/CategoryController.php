<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CreateCategory;
use App\Http\Requests\Category\UpdateCategory;
use App\Models\Category;
use App\Services\Category\CategoryServiceImplement;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    public $categoryService;

    public function __construct(CategoryServiceImplement $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $categories = $this->categoryService->search_category_name_and_description($request);

        session(['save_url' => $request->fullUrl()]);

        $stt = 1;

        return view('category.index', compact('categories', 'stt'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(CreateCategory $request)
    {
        $category = $this->categoryService->create($request->all());

        session()->flash('message', 'Create Category Success');
        session()->flash('message_class', 'alert-success');
        return redirect()->route('category.index');
    }

    public function edit($id)
    {
        $category = Category::FindOrFail($id);
        return view('category.edit', compact('category'));
    }

    public function show($id)
    {
        $category = Category::FindOrFail($id);
        return view('category.show', compact('category'));
    }

    public function update(UpdateCategory $request, $id)
    {
        $category = Category::FindOrFail($id);
        $category->update($request->all());
        session()->flash('message', 'Update Category Success');
        session()->flash('message_class', 'alert-warning');
        // goi lai biến lưu url để không bị thay đổi url
        $saveUrl = session('save_url', route('category.index'));
        return redirect($saveUrl);
    }

    public function delete(Request $request, $id)
    {
        $category = Category::FindOrFail($id);
        $category->delete($category);
        session()->flash('message', 'Delete Category Success');
        session()->flash('message_class', 'alert-danger');
        // goi lai biến lưu url để không bị thay đổi url
        $saveUrl = session('save_url', route('category.index'));
        return redirect($saveUrl);
    }
}
