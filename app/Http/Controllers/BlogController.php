<?php

namespace App\Http\Controllers;

use App\Events\BlogPusherEvent;
use Illuminate\Http\Request;
use App\Models\Blog;
use Pusher\Pusher;

class BlogController extends Controller
{

    public function index()
    {
        $blogs = Blog::all();

        return view('blog.index', compact('blogs'));
    }

    public function createBlog()
    {
        return view('blog.create');
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);

        $data['title'] = $request->input('title');
        $data['content'] = $request->input('content');

        $blog = Blog::Create($request->all());
        $options = array(
            'cluster' => 'ap1',
            'encrypted' => true
        );

        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $pusher->trigger('BlogPusherEvent', 'send-blog-pusher', $blog);

        return redirect()->route('blog.create');
    }

    public function getDataPosts()
    {
        // Lấy dữ liệu mới từ database, ví dụ:
        $latestBlogs = Blog::all();

        return response()->json(['data' => $latestBlogs]);
    }
}
