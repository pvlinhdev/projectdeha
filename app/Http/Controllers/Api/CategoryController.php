<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Category\StoreCategory;
use App\Http\Requests\Api\Category\UpdateCategory;
use App\Http\Resources\Category\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = Category::all();
        $categoryResource = CategoryResource::collection($categories);

        return response()->json([
            'message' => "List Category",
            'status' => Response::HTTP_OK,
            'data' => $categoryResource
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCategory $request)
    {
        $category = Category::create($request->all());
        $categoryResource = new CategoryResource($category);

        return response()->json([
            'message' => "Create success",
            'status' => Response::HTTP_CREATED,
            'data' => $categoryResource
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $category = Category::findOrFail($id);
        $categoryResource = new CategoryResource($category);

        return response()->json([
            'message' => "Show category success",
            'status' => Response::HTTP_OK,
            'data' => $categoryResource
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCategory $request, string $id)
    {
        $category = Category::findOrFail($id);
        $category->update($request->all());
        $categoryResource = new CategoryResource($category);

        return response()->json([
            'message' => "Update success",
            'status' => Response::HTTP_CREATED,
            'data' => $categoryResource
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $category = Category::findOrFail($id);

        $category->delete();

        return response()->json([
            'message' => "Delete success",
            'status' => Response::HTTP_NO_CONTENT,
            'data' => []
        ]);
    }

    /**
     * Count category.
     */
    public function count()
    {
        $cateogry = Category::all();
        $count = $cateogry->count();
        return response()->json([
            'message' => "Count success",
            'status' => Response::HTTP_OK,
            'data' => [
                'count-category' => $count
            ]
        ]);
    }
}
