<?php

namespace Tests\Feature\TDD;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewCategoryTest extends TestCase
{
    /**
     * @test
     */
    public function un_auth_user_can_new_product()
    {
        $category = Category::factory()->make()->toArray();

        $response = $this->post(route('category.store'), $category);

        $response->assertRedirect('login');
    }

    /**
     * @test
     */
    public function authenticated_user_can_new_create_category()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $category = Category::factory()->make()->toArray();

        $response = $this->post(route('category.store'), $category);

        $response->assertStatus(Response::HTTP_FOUND);

        $response->assertRedirect(route('category.index'));

    }

    /**
     * @test
     */
    public function auth_user_can_new_category_name_null()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $category = [
            'name' => "",
            'description' => 'Category Description',
        ];
        $response = $this->post(route('category.store'), $category);

        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function auth_user_can_new_category_description_null()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $category = [
            'name' => 'Category name',
            'description' => null,
        ];
        $response = $this->post(route('category.store'), $category);

        $response->assertSessionHasErrors(['description']);
    }

    /**
     * @test
     */
    public function auth_user_can_view_create_category()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->get(route('category.create'));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertViewIs('category.create');
    }
}
