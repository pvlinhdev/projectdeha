<?php

namespace Tests\Feature\TDD;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    /**
     * @test
     */
    public function auth_user_can_update_category_view()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $category = Category::factory()->create();

        $response = $this->put(route('category.update', ['id' => $category->id]));
        $response->assertStatus(Response::HTTP_FOUND);

        $this->assertDatabaseHas('categories', ['id' => $category->id]);

        $response->assertRedirect(route('category.index'));
    }

    /**
     * @test
     */
    public function auth_user_can_update_category_description_null()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $category = Category::factory()->create();

        $category->update(['description' => null]);

        $response = $this->put(route('category.update', ['id' => $category->id]), [
            'description' => null,
        ]);

        $response->assertRedirect(route('category.index'));

    }

    /**
     * @test
     */
    public function auth_user_can_update_category_name_null()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $category = Category::factory()->create();

        $response = $this->put(route('category.update', ['id' => $category->id]), [
            'name' => null,
        ]);

        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function auth_user_can_update_category_namesake()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        // $category = Category::factory()->create();

        $response = $this->put(route('category.update', ['id' => 128]), [
            'name' => 'Ericka Senger',
        ]);

        $response->assertSessionHasErrors(['name']);
    }
}
