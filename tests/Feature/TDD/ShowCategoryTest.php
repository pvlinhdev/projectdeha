<?php

namespace Tests\Feature\TDD;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowCategoryTest extends TestCase
{
    /**
     * @test
     */
    public function use_can_get_product_to_show(): void
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $category = Category::factory()->create();

        $response = $this->get(route('category.show', ['id' => $category->id]));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertViewIs('category.show');

    }
    /**
     * @test
     */
    public function un_auth_user_can_not_show_category()
    {
        $category = category::factory()->create();

        $response = $this->get(route('category.show', ['id' => $category->id]));

        $response->assertRedirect('/login');
    }
}
