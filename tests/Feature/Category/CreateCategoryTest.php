<?php

namespace Tests\Feature\Category;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    /**
     * @test
     */
    public function user_can_create_category_if_data_is_valid(): void
    {
        $dataCreate = [
            'name' => fake()->name(),
            'description' => fake()->text(),
        ];

        $response = $this->json('POST', route('categories.store'), $dataCreate);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has( 'data', fn (AssertableJson $json) =>
                    $json->where('name', $dataCreate['name'])->etc()
            )->etc()
        );

        $this->assertDatabaseHas('categories', [
            'name' => $dataCreate['name'],
            'description' => $dataCreate['description'],
        ]);
    }

    /**
     * @test
     */
    public function user_can_create_category_if_name_null()
    {
        $dataCreate = [
            'name' => '',
            'description' => fake()->text(),
        ];

        $response = $this->postJson(route('categories.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('name')
                    ->etc()
            )->etc()
        );
    }

    /**
     * @test
     */
    public function user_can_create_category_if_description_null()
    {
        $dataCreate = [
            'name' => fake()->name(),
            'description' => '',
        ];

        $response = $this->postJson(route('categories.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('description')
                    ->etc()
            )->etc()
        );
    }

    /**
     * @test
     */
    public function user_can_create_category_if_null()
    {
        $dataCreate = [
            'name' => '',
            'description' => '',
        ];

        $response = $this->postJson(route('categories.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('name')
                    ->has('description')
            )->etc()
        );
    }
}
