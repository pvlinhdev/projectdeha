<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetCategoryTest extends TestCase
{
    /**
     * @test
     */
    public function user_can_get_category_if_category_exists(): void
    {
        $category = Category::factory()->create();

        $response = $this->getJson(route('categories.show', $category->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has('message')
                ->has('status')
                ->has(
                    'data',
                    fn (AssertableJson $json) =>
                    $json->where('name', $category->name)
                        ->etc() #tại vì sau where còn description nên có etc chạy k lỗi
                )
        );
    }

    /**
     * @test
     */
    public function user_can_not_get_category_if_category_not_exists(): void
    {
        $category_id = -1;

        $response = $this->getJson(route('categories.show', $category_id));

        $response->assertStatus(Response::HTTP_NOT_FOUND);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has('message')
                ->has('status')
        );
    }
}
