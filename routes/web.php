<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FrontEndController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SendMessageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|---------------------  -----------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group(['middleware' => 'localization'], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/', [CategoryController::class, 'index'])->name('category.index');

    Route::group(['middleware' => ['auth']], function () {
        Route::group(['prefix' => '/category'], function () {
            Route::get('/create', [CategoryController::class, 'create'])->name('category.create');
            Route::post('/store', [CategoryController::class, 'store'])->name('category.store');
            Route::get('/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
            Route::put('/{id}', [CategoryController::class, 'update'])->name('category.update');
            Route::get('/show/{id}', [CategoryController::class, 'show'])->name('category.show');
            Route::delete('/delete/{id}', [CategoryController::class, 'delete'])->name('category.delete');
        });

        Route::group(['prefix' => '/blog'], function () {
            Route::get('/', [BlogController::class, 'index'])->name('blog.index');
            Route::get('/create', [BlogController::class, 'createBlog'])->name('blog.create');
            Route::post('/store', [BlogController::class, 'store'])->name('blog.store');
            Route::get('get-latest-data', [BlogController::class, 'getDataPosts'])->name('get.posts');
        });
    });

    Auth::routes();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('change-language/{language}', [HomeController::class, 'changeLanguage'])->name('change-language');
});
