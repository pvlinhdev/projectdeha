<?php
return [
    'Language' => 'Ngôn ngữ',
    'Category' => 'Danh mục',
    'Show Category' => 'Hiển thị danh mục',
    'Blog' => 'Bài viết',
    'Create Blog' => 'Thêm bài viết'
];
