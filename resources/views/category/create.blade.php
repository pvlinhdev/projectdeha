@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Category</div>
                    <form style="padding: 20px" action="{{ route('category.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputName">Name Category: </label>
                            <input type="name" class="form-control @error('name') is-invalid @enderror"
                                id="exampleInputName" aria-describedby="nameHelp" placeholder="Enter name" name="name" value="{{ old('name')  }}">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group mt-3">
                            <label for="exampleInputDescription">Description Category:</label>
                            <input type="description" class="form-control @error('description') is-invalid @enderror"
                                id="exampleInputDescription" placeholder="Enter description" name="description" value="{{ old('description') }}">
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-outline-success mt-3">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
