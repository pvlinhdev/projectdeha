@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">List Category
                        @if (Session::has('message'))
                            <div id="myAlert"
                                class="alert {{ Session::get('message_class', 'alert-success') }} alert-dismissible fade show"
                                role="alert">
                                {{ Session::get('message') }}
                            </div>
                            <script>
                                setTimeout(function() {
                                    document.getElementById('myAlert').style.display = 'none';
                                }, 3000);
                            </script>
                        @endif
                    </div>
                    <table class="table table-bordered">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th style="width: 250px" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $item)
                                <tr>
                                    <th scope="row">{{ $stt++ }}</th>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->description }}</td>
                                    <td class="d-flex justify-content-between">
                                        <a href="{{ route('category.edit', $item->id) }}"
                                            class="btn btn-outline-warning">Update</a>
                                        <a href="{{ route('category.show', $item->id) }}"
                                            class="btn btn-outline-info">Show</a>
                                        {{-- <form action="{{ route('category.delete', $item->id) }}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                                        </form> --}}
                                        <button type="button" class="btn btn-outline-danger" onclick="confirmDelete('{{ route('category.delete', $item->id) }}')">Delete</button>
                                        <form id="deleteForm" action="{{ route('category.delete', $item->id) }}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" style="display:none;" class="btn btn-outline-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-4">
                        {{ $categories->appends(request()->input())->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
