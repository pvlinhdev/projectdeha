@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> {{__('messages.Show Category')}} </div>
                    <form style="padding: 20px">
                        <div class="form-group">
                            <label for="exampleInputName">Name Category: </label>
                            <input type="name" class="form-control" id="exampleInputName" aria-describedby="nameHelp"
                                value="{{ $category->name }}" name="name" disabled>
                        </div>
                        <div class="form-group mt-3">
                            <label for="exampleInputDescription">Description Category:</label>
                            <textarea type="description" class="form-control"
                                id="exampleInputDescription" name="description" disabled>{{ $category->description }}</textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
