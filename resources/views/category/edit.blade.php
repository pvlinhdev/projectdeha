@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Update Category ({{ $category->name }}) </div>
                    <form style="padding: 20px" action="{{ route('category.update', $category->id) }}" method="POST">
                        @method('put')
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputName">Name Category: </label>
                            <input type="name" class="form-control @error('name') is-invalid @enderror"
                                id="exampleInputName" aria-describedby="nameHelp" value="{{ old('name', $category->name)  }}"
                                name="name">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group mt-3">
                            <label for="exampleInputDescription">Description Category:</label>
                            <input type="description" class="form-control @error('description') is-invalid @enderror"
                                id="exampleInputDescription" name="description" value="{{ old('description', $category->description)  }}"></input>
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-outline-primary mt-3">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
