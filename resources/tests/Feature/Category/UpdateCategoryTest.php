<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    /**
     * @test
     */
    public function user_can_update_category_if_data_is_valid(): void
    {
        $category = Category::factory()->create();
        $dataUpdate = [
            'name' => fake()->name(),
            'description' => fake()->text(),
        ];

        $response = $this->json('PUT', route('categories.update', $category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'data',
                fn (AssertableJson $json) =>
                $json->where('name', $dataUpdate['name'])
                    ->etc()
            )->etc()
        );
        // Check trong db có trường dữ liệu fake trên k
        $this->assertDatabaseHas('categories', [
            'name' => $dataUpdate['name'],
            'description' => $dataUpdate['description'],
        ]);
    }

    /**
     * @test
     */
    public function user_can_not_update_category_exists_name_null(): void
    {
        $category = Category::factory()->create();
        $dataUpdate = [
            'name' => '',
            'description' => fake()->text(),
        ];

        $response = $this->json('PUT', route('categories.update', $category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('name')
                    ->etc()
            )->etc()
        );
    }

    /**
     * @test
     */
    public function user_can_not_update_category_exists_description_null(): void
    {
        $category = Category::factory()->create();
        $dataUpdate = [
            'name' => fake()->name(),
            'description' => '',
        ];

        $response = $this->json('PUT', route('categories.update', $category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('description')
                    ->etc()
            )->etc()
        );
    }

    /**
     * @test
     */
    public function user_can_not_update_category_exists_name_and_description_null(): void
    {
        $category = Category::factory()->create();
        $dataUpdate = [
            'name' => '',
            'description' => '',
        ];

        $response = $this->json('PUT', route('categories.update', $category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('name')
                    ->has('description')
            )->etc()
        );
    }
    /**
     * @test
     */
    public function user_can_not_update_category_if_nor_exists_and_data_valid(): void
    {
        $categoryId = -1;
        $dataUpdate = [
            'name' => fake()->name(),
            'description' => fake()->text(),
        ];

        $response = $this->json('PUT', route('categories.update', $categoryId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }
}
