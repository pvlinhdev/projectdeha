<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /**
     * @test
     */
    public function user_can_delete_category_is_category_exists(): void
    {
        $category = Category::factory()->create();
        $categoryBeforeCount = Category::count();
        $response = $this->json('DELETE', route('categories.destroy', $category->id));

        $response->assertStatus(Response::HTTP_OK);

        $categoryCountAlterDelete = Category::count();
        // Kiem tra =
        $this->assertEquals($categoryBeforeCount - 1, $categoryCountAlterDelete);
    }
    /**
     * @test
     */
    public function user_can_delete_category_is_category_not_exists(): void
    {
        $categoryId = -1;

        $response = $this->json('DELETE', route('categories.destroy', $categoryId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
