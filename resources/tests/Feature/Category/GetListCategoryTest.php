<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    /** @test */
    public function user_can_get_list_categories(): void
    {
        $categoryCount = Category::count();
        $response = $this->getJson(route('categories.index'));
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('message')
            ->has('status')
            ->has('data')
        );
    }
}
