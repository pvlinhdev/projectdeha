<?php

namespace Tests\Feature\TDD;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    /**
     * @test
     */
    public function user_can_get_all_categories(): void
    {
        $category = Category::factory()->create();
        $response = $this->get(route('category.index'));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('category.index');
        $response->assertSee($category->name);
    }
}
