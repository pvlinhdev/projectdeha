<?php

namespace Tests\Feature\TDD;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchCategoryTest extends TestCase
{

    /**
     * @test
     */
    public function user_can_category_equal_name(): void
    {
        $categoryName = 'Mr';

        $category = Category::factory()->create();
        $response = $this->get(route('category.search',['name' => $categoryName]));

        $response->assertStatus(200);

        $response->assertSee($categoryName);
    }

    /**
     * @test
     */
    public function user_can_category_equal_name_null(): void
    {
        $categoryName = '';

        $category = Category::factory()->create();
        $response = $this->get(route('category.search',['name' => $categoryName]));

        $response->assertStatus(200);

        $response->assertSee($categoryName);
    }
}
