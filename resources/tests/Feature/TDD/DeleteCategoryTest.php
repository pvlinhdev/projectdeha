<?php

namespace Tests\Feature\TDD;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /**
     * @test
     */
    public function auth_can_delete_category(): void
    {
        // Tạo một đối tượng người dùng xác thực
        $user = User::factory()->create();

        // Đăng nhập người dùng
        $this->actingAs($user);

        $category = Category::factory()->create();

        $response = $this->delete(route('category.delete', ['id' => $category->id]));

        $response->assertStatus(Response::HTTP_FOUND);

        $response->assertRedirect(route('category.index'));
    }

    /**
     * @test
     */
    public function un_auth_user_can_not_delete_form_view()
    {
        $category = category::factory()->create();

        $response = $this->delete(route('category.delete', ['id' => $category->id]));

        $response->assertRedirect('/login');
    }
}
